class EvalD implements ExprVisitorI {
    public Object forPlus(ExprD l, ExprD r) {
        return plus(l.accept(this), r.accept(this)); }
    public Object forDiff(ExprD l, ExprD r) {
        return diff(l.accept(this), r.accept(this)); }
    public Object forProd(ExprD l, ExprD r) {
        return prod(l.accept(this), r.accept(this)); }
    public Object forConst(Object c) {
        return c; }
    abstract Object plus(Object l, Object r);
    abstract Object diff(Object l, Object r);
    abstract Object prod(Object l, Object r);
}

class IntEvalV extends ExprVisitorI {
    Object plus(Object l, Object r) { return new Integer(
            ((Integer)l).intValue() +
            ((Integer)r).intValue() ); }
    Object diff(Object l, Object r) { return new Integer(
            ((Integer)l).intValue() -
            ((Integer)r).intValue() ); }
    Object prod(Object l, Object r) { return new Integer(
            ((Integer)l).intValue() *
            ((Integer)r).intValue() ); }
}

class SetEvalV extends ExprVisitorI {
    Object plus(Object l, Object r) { return ((setD)l).plus((SetD)r); }
    Object diff(Object l, Object r) { return ((setD)l).diff((SetD)r); }
    Object prod(Object l, Object r) { return ((setD)l).prod((SetD)r); }
}

