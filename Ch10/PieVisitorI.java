
interface PieVisitorI {
    Object forBot(Bot that);
    Object forTop(Top that);
}

class OccursV implements PieVisitorI {
    Object a;
    OccursV(Object _a) { a=_a; }
    // -------------------------
    public Object forBot(Bot that) {
        return new Integer(0); }
    public Object forTop(Top that) {
        if (that.t.equals(a))
            return
                new Integer(((Integer)
                            (that.r.accept(this)))
                        .intValue()
                        + 1);
        else
            return that.r.accept(this); }
}

class RemV implements PieVisitorI {
    Object o;
    RemV(Object _o) { o=_o; }
    // ----------------------
    public Object forBot(Bot that) {
        return new Bot(); }
    public Object forTop(Top that) {
        if (o.equals(that.t))
            return that.r.accept(this);
        else
            return 
                new Top(that.t,
                        (PieD)that.r.accept(this));
    }
}

class SubstV implements PieVisitorI {
    Object n; Object o;
    SubstV(Object _n, Object _o) {
        n=_n; o=_o; }
    // -------------------------
    public Object forBot(Bot that) {
        return that; }
    public Object forTop(Top that) {
        if (o.equals(that.t)) {
            that.t = n
                ;
            that.r.accept(this)
                ;
            return that; }
        else {
            that.r.accept(this)
                ;
            return that; }
    }
}
