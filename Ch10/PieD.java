
abstract class PieD {
    abstract Object accept(PieVisitorI ask);
    // ---------------------------------------
    abstract public String toStringS(String ind);
    public String getName(String fields) { return "new " + getClass().getName() + "(" + fields + ")"; }
}

class Bot extends PieD {
    Object accept (PieVisitorI ask) { return ask.forBot(this); }
    // ---------------------------------------
    public String toStringS(String ind) { return getName(""); }
}

class Top extends PieD {
    PieD r;
    Object t;
    Top(Object _t, PieD _p) { r = _p; t = _t; } 
    // ----------------------------------------
    Object accept (PieVisitorI ask) { return ask.forTop(this); }
    // -------------------------------------------------------
    public String toStringS(String ind) { return getName(
            t.toString() + ",\n" +
            ind + r.toStringS(" "+ind) ); }
}
