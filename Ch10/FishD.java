
abstract class FishD {
    public String getName(String fields) { 
        return "new " + getClass().getName() + "(" + fields + ")"; }
    public String toString() { return getName(""); }
}

class Anchovy extends FishD {
    public boolean equals(Object o) {
        return (o instanceof Anchovy);
    }
}

class Salmon extends FishD {
    public boolean equals(Object o) {
        return (o instanceof Salmon);
    }
}

class Tuna extends FishD {
    public boolean equals(Object o) {
        return (o instanceof Tuna);
    }
}
