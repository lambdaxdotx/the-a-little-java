
abstract class PieD {
    RemAV    raFn    = new RemAV();
    RemFishV rfFn    = new RemFishV();
    RemV     remFn   = new RemV();
    SubstV   substFn = new SubstV();
    abstract PieD remA   ();
    abstract PieD remFish(FishD f);
    abstract PieD rem    (Object o);
    abstract PieD subst  (Object n, Object o);
    // ---------------------------------------
    abstract public String toStringS(String ind);
    public String getName(String fields) { return "new " + getClass().getName() + "(" + fields + ")"; }
}

class Bot extends PieD {
    PieD remA   ()          { return raFn.forBot(); }
    PieD remFish(FishD f)   { return rfFn.forBot(f); }
    PieD rem    (Object o)  { return remFn.forBot(o); }
    PieD subst  (Object n,
                 Object o)  { return substFn.forBot(n,o); }
    // ---------------------------------------
    public String toStringS(String ind) { return getName(""); }
}

class Top extends PieD {
    PieD r;
    Object t;
    Top(Object _t, PieD _p) {
        r = _p; 
        t = _t;
    } // --------------------
    PieD remA   ()          { return raFn.forTop(t,r); }
    PieD remFish(FishD f)   { return rfFn.forTop(t,r,f); }
    PieD rem    (Object o)  { return remFn.forTop(t,r,o); }
    PieD subst  (Object n,
                 Object o)  { return substFn.forTop(t,r,n,o); }
    // ---------------------------------------
    public String toStringS(String ind) { return getName( t.toString() + ",\n" +
            ind + r.toStringS(" "+ind) ); }
}
