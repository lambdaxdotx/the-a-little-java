
class Main {
    public static void main(String args[]) {
        /* Here create the object y with which you wish to experiment ...  */

        System.out.println("\np.70");
        PieD z70 = 
            new Top(new Salmon(),
                    new Top(new Anchovy(),
                        new Top(new Tuna(),
                            new Top(new Anchovy(),
                                new Bot()))));
        System.out.println(z70.toStringS(" "));
        System.out.println(".remA()?");
        System.out.println(z70.remA().toStringS(" "));


        System.out.println("\np.75");
        PieD z75 = 
            new Top(new Anchovy(),
                    new Bot());
        System.out.println(z75.toStringS(" "));
        System.out.println(".remFish(new Anchovy())?");
        System.out.println(z75.remFish(new Anchovy()).toStringS(" "));
 
        System.out.println("\np.78");
        PieD z78 = 
            new Top(new Integer(2),
                    new Top(new Integer(3),
                        new Top(new Integer(2),
                            new Bot())));
        System.out.println(z78.toStringS(" "));
        System.out.println(".rem(new Integer(3))?");
        System.out.println(z78.rem(new Integer(3)).toStringS(" "));
 
        System.out.println("\np.79");
        PieD z79 = 
            new Top(new Anchovy(),
                    new Top(new Integer(3),
                        new Top(new Zero(),
                            new Bot())));
        System.out.println(z79.toStringS(" "));
        System.out.println(".rem(new Zero())?");
        System.out.println(z79.rem(new Zero()).toStringS(" "));

        System.out.println("\np.81");
        PieD z81 = 
            new Top(new Anchovy(),
                    new Top(new Tuna(),
                        new Top(new Anchovy(),
                            new Bot())));
        System.out.println(z81.toStringS(" "));
        System.out.println(".subst(new Salmon(), new Anchovy())?");
        System.out.println(z81.subst(
                    new Salmon(), 
                    new Anchovy()).toStringS(" "));

        System.out.println("\np.82");
        PieD z82 = 
            new Top(new Integer(3),
                    new Top(new Integer(2),
                        new Top(new Integer(3),
                            new Bot())));
        System.out.println(z82.toStringS(" "));
        System.out.println(".subst(new Integer(5), new Integer(3))?");
        System.out.println(z82.subst(
                    new Integer(5), 
                    new Integer(3)).toStringS(" "));

        /* --- --- THE SUN set, but set not his hope --- --- */
        /*
           abstract public String toStringS(String ind);
           public String getName(String fields) { return "new " + getClass().getName() + "(" + fields + ")"; }
           public String toStringS(String ind) { return getName(""); }
           public String toStringS(String ind) { return getName("\n"+ind + s.toStringS(" "+ind)); }
        */
    }
}
