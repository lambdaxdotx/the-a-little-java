/*
 * NumD
 *  |<-- Zero
 *  |<-- OneMoreThan
 */

abstract class NumD {
    abstract public String toString();
}

class Zero extends NumD {
    public boolean equals(Object o) {
        return (o instanceof Zero); }
    public String toString() { return "new " + getClass().getName() + "(" + ")"; }
}

class OneMoreThan extends NumD {
    NumD predecessor;
    OneMoreThan(NumD _p) { predecessor = _p; }
    // ---------------------------------------
    public boolean equals(Object o) {
        if (o instanceof OneMoreThan) {
            return // this == o iff this.predecessor == o.predecessor
                predecessor.equals( ((OneMoreThan)o).predecessor );
        } else {
            return false;
        } }
    public String toString() { return "1+" + predecessor.toString(); }
}
