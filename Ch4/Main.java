
class Main {
    public static void main(String args[]) {
        /* Here create the object y with which you wish to experiment ...  */

        System.out.println("\nx");
        Onion x = 
            new Onion(
                    new Onion(
                        new Skewer()));
        System.out.println(x.toStringS(" "));
        System.out.println(".onlyOnions()?");
        System.out.println(x.onlyOnions());

        System.out.println("\ny");
        Onion y = 
            new Onion(
                    new Tomato(
                        new Onion(
                            new Skewer())));
        System.out.println(y.toStringS(" "));
        System.out.println(".isVegetarian()?");
        System.out.println(y.isVegetarian());

        System.out.println("\np.51");
        Olive z51 = 
            new Olive(
                new Anchovy(
                    new Cheese(
                        new Anchovy(
                            new Crust()))));
        System.out.println(z51.toStringS(" "));
        System.out.println(".topAwC().remA()?");
        System.out.println(z51.topAwC().remA().toStringS(" "));

        /* --- --- THE SUN set, but set not his hope --- --- */
        /*
           abstract public String toStringS(String ind);
           public String getName(String fields) { return "new " + getClass().getName() + "(" + fields + ")"; }
           public String toStringS(String ind) { return getName(""); }
           public String toStringS(String ind) { return getName("\n"+ind + s.toStringS(" "+ind)); }
        */
    }
}
