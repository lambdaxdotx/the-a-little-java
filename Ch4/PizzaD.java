/* PizzaD
 *   | <-- Crust   [Object o] 
 *   | <-- Cheese  [PizzaD s]
 *   | <-- Olive   [PizzaD s]
 *   | <-- Anchovy  [PizzaD s]
 *   | <-- Sausage  [PizzaD s]
 *   | <-- Zucchini [PizzaD s]
 */

abstract class PizzaD {
    // -------------------------
    RemAV remFn =   new RemAV();
    TopAwCV topFn = new TopAwCV();
    SubAbCV subFn = new SubAbCV();
    abstract PizzaD remA()   ;
    abstract PizzaD topAwC() ;
    abstract PizzaD subAbC() ;
    abstract public String toStringS(String ind);
    public String getName(String fields) { return "new " + getClass().getName() + "("+fields+")"; }
}

class Crust extends PizzaD {
    // -------------------------
    PizzaD remA()   { return remFn.forCrust(); }
    PizzaD topAwC() { return topFn.forCrust(); }
    PizzaD subAbC() { return subFn.forCrust(); }
    public String toStringS(String ind) { return getName(""); }
}

class Cheese extends PizzaD {
    PizzaD p;
    Cheese(PizzaD _p) { p = _p; }
    // -------------------------
    PizzaD remA()   { return remFn.forCheese(p); }
    PizzaD topAwC() { return topFn.forCheese(p); }
    PizzaD subAbC() { return subFn.forCheese(p); }
    public String toStringS(String ind) { return getName("\n"+ind + p.toStringS(" "+ind)); }
}

class Olive extends PizzaD {
    PizzaD p;
    Olive(PizzaD _p) { p = _p; }
    // -------------------------
    PizzaD remA()   { return remFn.forOlive(p); }
    PizzaD topAwC() { return topFn.forOlive(p); }
    PizzaD subAbC() { return subFn.forOlive(p); }
    public String toStringS(String ind) { return getName("\n"+ind + p.toStringS(" "+ind)); }
}

class Anchovy extends PizzaD {
    PizzaD p;
    Anchovy(PizzaD _p) { p = _p; }
    // -------------------------
    PizzaD remA()   { return remFn.forAnchovy(p); }
    PizzaD topAwC() { return topFn.forAnchovy(p); }
    PizzaD subAbC() { return subFn.forAnchovy(p); }
    public String toStringS(String ind) { return getName("\n"+ind + p.toStringS(" "+ind)); }
}

class Sausage extends PizzaD {
    PizzaD p;
    Sausage(PizzaD _p) { p = _p; }
    // -------------------------
    PizzaD remA()   { return remFn.forSausage(p); }
    PizzaD topAwC() { return topFn.forSausage(p); }
    PizzaD subAbC() { return subFn.forSausage(p); }
    public String toStringS(String ind) { return getName("\n"+ind + p.toStringS(" "+ind)); }
}

