
/* Chatper 5 : Object are people too */

class Main {
    public static void main(String args[]) {
        /* Here create the object y with which you wish to experiment ...  */
        System.out.println("\np.70");
        PieD z =
            new Top(new Anchovy(), 
                    new Top(new Tuna(), 
                        new Top(new Anchovy(), 
                                new Bot())));
        System.out.println( z.toStringS(" ") );
        System.out.println( "a pizza PieD? : " + (z instanceof PieD) );

//        System.out.println("\np.82");
//        System.out.println(
//                new Top(new Integer(3),
//                    new Top(new Integer(2),
//                        new Top(new Integer(3),
//                            new Bot())))
//                .subst(new Integer(5), new Integer(3)).toStringS(" "));
//
//        System.out.println("\np.**");
//        System.out.println(
//                new Top(new Anchovy(),
//                    new Top(new Anchovy(),
//                        new Top(new Salmon(),
//                            new Top(new Anchovy(),
//                                new Bot()))))
//                .rem(new Anchovy()).toStringS(" "));

        System.out.println("\np.96");
        System.out.println(
                new Top(new Anchovy(),
                    new Top(new Tuna(),
                        new Top(new Anchovy(),
                            new Top(new Tuna(),
                                new Top(new Anchovy(),
                                    new Bot())))))
            .accept(new LtdSubstV(2,
                        new Salmon(),
                        new Anchovy()))
            .toStringS(" "));
        /* --- --- ---    THE SUN set, but set not his hope    --- --- --- */
        /*
        public String toString() { return "new " + getClass().getName() + "(" + ")"; }
        public String toString() { return "new " + getClass().getName() + "(" + x + ")"; }
        public String toString() { return "new " + getClass().getName() + "(" + x + ", " + y + ")"; }
        */
    }
}
