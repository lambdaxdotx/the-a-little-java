
class Top extends PieD {
    PieD r;
    Object t;
    Top(Object _t, PieD _p) {
        r = _p; 
        t = _t;
    } // --------------------
    PieD accept(PieVisitorI ask) { return ask.forTop(t, r); }
    public String toString() { return "new " + getClass().getName() + "(" + "\n" + t.toString() + ", " + r.toString() + ")"; }
    public String toStringS(String s) { return "new " + getClass().getName() + "(" + t.toString() + ",\n" + s + r.toStringS(" "+s) + ")"; }
}
