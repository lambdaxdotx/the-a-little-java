
abstract class PieD {
    abstract PieD accept(PieVisitorI ask);
    abstract public String toStringS(String s);
}
