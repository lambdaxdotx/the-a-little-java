
class RemV implements PieVisitorI {
    Object o;
    RemV(Object _o) { o = _o; }
    // ------------------------
    public PieD forBot() { return new Bot(); }
    public PieD forTop(Object t, PieD r) {
        if (o.equals(t)) {
            return r.accept(this);
        } else {
            return new Top(t, r.accept(this));
        }
    }
}
