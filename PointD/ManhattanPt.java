
class ManhattanPt extends PointD {
    ManhattanPt(int _x, int _y) { super(_x, _y); }
    // -------------------------------------------
    int distanceToO() { return x + y; }
    public String toString() { return "new " + getClass().getName() + "(" + x + ", " + y + ")"; }
}
