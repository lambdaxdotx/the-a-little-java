
class ShadowedManhattanPt extends ManhattanPt {
    int dx; int dy;
    ShadowedManhattanPt(int _x, int _y, int _dx, int _dy) {
        super(_x, _y);
        dx = _dx; dy = _dy; }
    // ----------------------------------------------------
    int distanceToO() { return super.distanceToO() + dx + dy; }
    public String toString() { return "new " + getClass().getName() + "(" + dx + ", " + dy + ")"; }
}
