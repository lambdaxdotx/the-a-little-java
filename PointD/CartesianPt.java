
class CartesianPt extends PointD {
    CartesianPt(int _x, int _y) { super(_x, _y); }
    // -------------------------------------------
    int distanceToO() { return (int)Math.sqrt( x*x + y*y ); }
    public String toString() { return "new " + getClass().getName() + "(" + x + ", " + y + ")"; }
}
