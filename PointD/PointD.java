
abstract class PointD {
    int x; int y;
    PointD(int _x, int _y) { x = _x; y = _y; }
    // ---------------------------------------
    boolean closerToO(PointD p) {
        return distanceToO() <= p.distanceToO(); }
    PointD  minus(PointD p) {
        return CartesianPt(x-p.x, y-p.y); }
    abstract int distanceToO();
}


