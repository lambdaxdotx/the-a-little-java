

class UnionHasPtV extends HasPtV implements UnionVisitorI {
    UnionHasPtV(PointD _p) { super(_p); }
    // --------------------------
    public boolean forUnion(ShapeD s, ShapeD t) {
        return s.accept(this) && t.accept(this); }
}
