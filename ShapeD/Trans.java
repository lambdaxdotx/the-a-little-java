
class Trans extends ShapeD { // Translation
    PointD q;
    ShapeD s;
    Trans(PointD _q, ShapeD _s) { q = _q; s = _s; }
    // --------------------------------------------
    boolean accept(ShapeVisitorI ask) { return ask.forTrans(q, s); }
    public String toStringS(String str) { return "new " + getClass().getName() + "(\n" + str + q.toString() + ",\n" + str + s.toStringS(" "+str) + ")"; }
}
