
class Circle extends ShapeD {
    int r;
    Circle(int _r) { r = _r; }
    // ----------------------
    boolean accept(ShapeVisitorI ask) { return ask.forCircle(r); }
    public String toStringS(String str) { return "new " + getClass().getName() + "(" + r + ")"; }
}
