
/* Chatper 9 */

class Main {
    public static void main(String args[]) {
        /* Here create the object y with which you wish to experiment ...  */

        System.out.println("\np.151");
        System.out.println(
                new Trans(
                    new CartesianPt(12, 2),
                    new Union(
                        new Square(10),
                        new Trans(
                            new CartesianPt(4, 4),
                            new Circle(5))))
                .toStringS(" "));

        System.out.println("\np.152");
        System.out.println(
                new Trans(
                    new CartesianPt(3, 7),
                    new Union(
                        new Square(10),
                        new Circle(10)))
                .accept(
                    new UnionHasPtV( new CartesianPt(13, 17))));
        
        /* --- --- ---    THE SUN set, but set not his hope    --- --- --- */
        /*
        public String toString() { return "new " + getClass().getName() + "(" + ")"; }
        public String toString() { return "new " + getClass().getName() + "(" + x + ")"; }
        public String toString() { return "new " + getClass().getName() + "(" + x + ", " + y + ")"; }
        */
        /*
        abstract public String toStringS(String s);
        public String toStringS(String s) { return "new " + getClass().getName() + "(" + ")"; }
        public String toStringS(String s) { return "new " + getClass().getName() + "(" + t.toString() + ",\n" + s + r.toStringS(" "+s) + ")"; }
        public String toStringS(String s) { return "new " + getClass().getName() + "(\n" + l.toStringS(" "+s) + ",\n" + s + r.toStringS(" "+s) + ")"; }
        */
    }
}
