

class HasPtV implements ShapeVisitorI {
    PointD p;
    HasPtV(PointD _p) { p = _p; }
    // --------------------------
    public boolean forCircle(int r) {
        return p.distanceToO() <= r; }
    public boolean forSquare(int s) {
        return (p.x <= s) && (p.y <= s); }
    public boolean forTrans(PointD q, ShapeD s) {
        return s.accept(new HasPtV(p.minus(q))); }
}
