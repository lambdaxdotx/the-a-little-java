
class Union extends ShapeD {
    ShapeD s; ShapeD t;
    Union(ShapeD _s, ShapeD _t) { s = _s; t = _t; }
    // --------------------------------------------
    boolean accept(ShapeVisitorI ask) {
        return ((UnionVisitorI)ask).forUnion(s, t);
    }
    public String toStringS(String str) { return "new " + getClass().getName() + "(\n" + str + s.toStringS(" "+str) + ",\n" + str + t.toStringS(" "+str) + ")"; }
}
