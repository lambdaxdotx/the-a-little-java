
abstract class ShapeD {
    abstract boolean accept(ShapeVisitorI ask);
    abstract public String toStringS(String str);
}
