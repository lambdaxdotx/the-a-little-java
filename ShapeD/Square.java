
class Square extends ShapeD {
    int s;
    Square(int _s) { s = _s; }
    // ----------------------
    boolean accept(ShapeVisitorI ask) { return ask.forSquare(s); }
    public String toStringS(String str) { return "new " + getClass().getName() + "(" + s + ")"; }
}
