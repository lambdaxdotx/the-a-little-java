2) Java Syntax
==============

A Top Down View
---------------

#. A Java program  consist of one ore more *compilation units* (files) of Java source code.
#. Each compilation unit begins with an (optional) **package** declaration followed by zero or more **import** declaration. In turn, the package and import are followed by zero of more *reference type definitions*.
#. Most of these definitions are either **class** or **interface**.
#. Within the definition of a reference type, we will have *members* such as:

   * *fields*
   * *methods*, and
   * *constructors*.

#. Methods, which are the most important kind of member, are blocks of Java code comprised of *statements*.

Lexical Structure
-----------------

 :The Unicode Character Set:
 :Case Sensitivity and Whitespace:

   * Java ignores spaces, tabs, newlines, and other whitespace, except when it appears within quoted characters and string literals.

 :Comments:
 :Reserved Words:
 :Identifiers:
 :Literals:
 :Punctuation: There are two categories of them.

   * Separators, () { } :: ; , . and
   * Operators + - * ...

Primitive Data Types
--------------------

#. boolean
#. char
#. byte
#. short
#. int
#. long
#. float
#. double

Expressions and Operators
-------------------------

  Till now, we have two categories of *tokens*:

    :literals: as primitive values in a Java program, and
    :variables: as symbolic names that represent, or holds, values,

  out of which our Java programs are built.

  Now we are going to next higher level of structure, *expressions*, in a Java program.

  The Java \`interpreter\` *evaluates* an expression to compute its value.

  The *primary expressions* are exactly literals and variables.

    * The resulting value of a literal is the literal itself.
    * The resulting value of a variable is the value stored in the variable.

  More complex expressions are made by using *operators* to combine primary expressions and we can do it recursively.

  Characters of **Operators**

    #. Precedence
    #. Associativity
    #. Operand number and type
    #. Return type
    #. *Side effects*
    #. Order of evaluation
    #. Categories
         :Arithmetic Operators:    ::  

             + - * / % -(unary minus)
         
         :String Concatenation Operator:
         :Increment, Decrement Operators:

            ++ -- 

         :Comparison Operators:  ::

            == != < <= > >=

         :Boolean Operators:  ::

            && || ! 
            & | ^ (always evaluate both operands!)

         :Bitwise and Shift Operators: (seldom used in Java)

         :Assignment Operators: 
            * The left operand must evaluate to an appropriate local variable, array element, or object field.
            * The right operand can be any value of a type compatible with the variable.
            * An assignment expression (itself) evaluates to the value that is assigned to the variable.
            * Most importantly, the expression has the *side effect* of actually performing the assignment.

         :The Conditional Operator: ?:

         :The instanceof Operator: 
         
         :Special Operators: 
            #. Object member access .
            #. Array element access []
            #. Method invocation ()
            #. Lambda expression ->
            #. Object creation  `new`
            #. Type conversion, casting ()

Statements
==========

  * A *statement* is a basic unit of execution in the Java language. It expresses a single piece of intent by the programmer.
  * Unlike expressions, Java statements do not have a value.
  * Statements also typically contain expressions and operators (especially assignment) and are frequently executes for the *side effects* that they cause.

  #. **Expression Statements**
       * Certain types of Java expressions have side effects: they do not simply *evaluate* to some value; they also change the *program state* in some way.
       * Any expression with side effects can be used as a statement simply following it with a semicolon. Examples are
           * assignments, =
           * increments and decrements, ++ --
           * method calls, and ()
           * object creation. `new`
  #. **Compound Statements** :any number and kind of statements grouped together within curly braces.
  #. **The Empty Statement**
  #. **Labeled Statements** simply a statement that has been given a name by prepending an identifier and a colon to it.
  #. **Local Variable Declaration Statements**
  #. **The if/else Statement** : The *if* statement has an associated expression and statement. If the expression evaluates to *true*, the interpreter executes the statement. If the expression evaluates to *false*, the interpreter skips the statement.
  #. **The switch Statement** ::

       while (expression)
         statement

  #. **The do Statement** ::

       do
         statement
       while (expression);

  #. **The for Statement** ::

       for (initialize; test; update)
         statement

     is basically equivalent to the following ::

       initialize;
       while (test) {
         statement;
         update;
       }

    The interpreter discards the values of the *initialize* and *update* expressions, so to be useful, these expressions must have side effects.

  #. **The foreach Statement**
  #. **The jump Statements**

    #. **The continue Statement**
    #. **The break Statement**
    #. **The return Statement**

  #. **The synchronized Statement**
  #. **The throw Statement**
  #. **The try/catch/finally Statement**
  #. **The assert Statement**

Methods
=======

