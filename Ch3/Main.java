
class Main {
    public static void main(String args[]) {
        /* Here create the object y with which you wish to experiment ...  */
        System.out.println("\np.44");
        Sausage z44 = 
            new Sausage(
                new Olive(
                    new Anchovy(
                        new Sausage(
                            new Cheese(
                                new Crust())))));
        System.out.println(z44.toStringS(" "));
        System.out.println(".remA()?");
        System.out.println(z44.remA().toStringS(" "));

        System.out.println("\np.49");
        Olive z49 = 
            new Olive(
                new Anchovy(
                    new Cheese(
                        new Anchovy(
                            new Crust()))));
        System.out.println(z49.toStringS(" "));
        System.out.println(".topAwC()?");
        System.out.println(z49.topAwC().toStringS(" "));

        System.out.println("\np.51");
        Olive z51 = 
            new Olive(
                new Anchovy(
                    new Cheese(
                        new Anchovy(
                            new Crust()))));
        System.out.println(z51.toStringS(" "));
        System.out.println(".topAwC().remA()?");
        System.out.println(z51.topAwC().remA().toStringS(" "));
        /* --- --- THE SUN set, but set not his hope --- --- */
        /*
           abstract public String toStringS(String ind);
           public String getName(String fields) { return "new " + getClass().getName() + "(" + fields + ")"; }
           public String toStringS(String ind) { return getName(""); }
           public String toStringS(String ind) { return getName("\n"+ind + s.toStringS(" "+ind)); }
        */
    }
}
