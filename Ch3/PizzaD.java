/* PizzaD
 *   | <-- Crust   [Object o] 
 *   | <-- Cheese  [PizzaD s]
 *   | <-- Olive   [PizzaD s]
 *   | <-- Anchovy  [PizzaD s]
 *   | <-- Sausage  [PizzaD s]
 *   | <-- Zucchini [PizzaD s]
 *   | <-- Spinach [PizzaD s]
 */

abstract class PizzaD {
    // -------------------------
    abstract PizzaD remA();
    abstract PizzaD topAwC();
    abstract PizzaD subAbC();
    abstract public String toStringS(String ind);
    public String getName(String fields) { return "new " + getClass().getName() + "("+fields+")"; }
}

class Crust extends PizzaD {
    // -------------------------
    PizzaD remA()   { return new Crust(); }
    PizzaD topAwC() { return new Crust(); }
    PizzaD subAbC() { return new Crust(); }
    public String toStringS(String ind) { return getName(""); }
}

class Cheese extends PizzaD {
    PizzaD p;
    Cheese(PizzaD _p) { p = _p; }
    // -------------------------
    PizzaD remA()   { return new Cheese(p.remA()); }
    PizzaD topAwC() { return new Cheese(p.topAwC()); }
    PizzaD subAbC() { return new Cheese(p.subAbC()); }
    public String toStringS(String ind) { return getName("\n"+ind + p.toStringS(" "+ind)); }
}

class Olive extends PizzaD {
    PizzaD p;
    Olive(PizzaD _p) { p = _p; }
    // -------------------------
    PizzaD remA()   { return new Olive(p.remA()); }
    PizzaD topAwC() { return new Olive(p.topAwC()); }
    PizzaD subAbC() { return new Olive(p.subAbC()); }
    public String toStringS(String ind) { return getName("\n"+ind + p.toStringS(" "+ind)); }
}

class Anchovy extends PizzaD {
    PizzaD p;
    Anchovy(PizzaD _p) { p = _p; }
    // -------------------------
    PizzaD remA()   { return                        p.remA(); }
    PizzaD topAwC() { return new Cheese(new Anchovy(p.topAwC())); }
    PizzaD subAbC() { return             new Cheese(p.subAbC()); }
    public String toStringS(String ind) { return getName("\n"+ind + p.toStringS(" "+ind)); }
}

class Sausage extends PizzaD {
    PizzaD p;
    Sausage(PizzaD _p) { p = _p; }
    // -------------------------
    PizzaD remA()   { return new Sausage(p.remA()); }
    PizzaD topAwC() { return new Sausage(p.topAwC()); }
    PizzaD subAbC() { return new Sausage(p.subAbC()); }
    public String toStringS(String ind) { return getName("\n"+ind + p.toStringS(" "+ind)); }
}

class Spinach extends PizzaD {
    PizzaD p;
    Spinach(PizzaD _p) { p = _p; }
    // -------------------------
    PizzaD remA()   { return new Spinach(p.remA()); }
    PizzaD topAwC() { return new Spinach(p.topAwC()); }
    PizzaD subAbC() { return new Spinach(p.subAbC()); }
    public String toStringS(String ind) { return getName("\n"+ind + p.toStringS(" "+ind)); }
}
