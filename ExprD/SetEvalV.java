
class SetEvalV extends EvalD {
    Object plus(Object l, Object r) { return 
            ((SetD)l).plus((SetD)r); }
    Object diff(Object l, Object r) { return
            ((SetD)l).diff((SetD)r); }
    Object prod(Object l, Object r) { return
            ((SetD)l).prod((SetD)r); }
}
