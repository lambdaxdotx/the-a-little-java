
class Diff extends ExprD {
    ExprD l; ExprD r;
    Diff(ExprD _l, ExprD _r) { l = _l; r = _r; } //-------
    Object accept(ExprVisitorI ask) { return ask.forDiff(l, r); }
}
