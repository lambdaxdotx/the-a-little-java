
class Plus extends ExprD {
    ExprD l; ExprD r;
    Plus(ExprD _l, ExprD _r) { l = _l; r = _r; } //-------
    Object accept(ExprVisitorI ask) { return ask.forPlus(l, r); }
}
