
class Empty extends SetD {
    boolean mem(Integer i) { return false; }
    SetD plus(SetD s) { return s; }
    SetD diff(SetD s) { return new Empty(); }
    SetD prod(SetD s) { return new Empty(); }
    public String toStringS(String str) { return "new " + getClass().getName() + "(" + ")"; }
}
