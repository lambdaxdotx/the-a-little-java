
class Const extends ExprD {
    Object c;
    Const(Object _c) { c = _c; } //------------------------
    Object accept(ExprVisitorI ask) { return ask.forConst(c); }
}
