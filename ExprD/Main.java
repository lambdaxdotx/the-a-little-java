
/* Chatper 8 */

class Main {
    public static void main(String args[]) {
        /* Here create the object y with which you wish to experiment ...  */

        System.out.println("\np.127");

        SetD x = (SetD)
            new Plus(
                    new Const(new Empty().add(new Integer(7))),
                    new Const(new Empty().add(new Integer(3))))
            .accept(new SetEvalV());
        System.out.println(x.toStringS(" "));

        SetD y = (SetD)
            new Diff(
                    new Const(new Empty().add(new Integer(7))),
                    new Const(new Empty().add(new Integer(3))))
            .accept(new SetEvalV());
        System.out.println(y.toStringS(" "));

        SetD z = (SetD)
            new Prod(
                    new Const(new Empty().add(new Integer(7))),
                    new Const(new Empty().add(new Integer(3))))
            .accept(new SetEvalV());
        System.out.println(z.toStringS(" "));

        System.out.println(
                ((SetD)
                 new Plus(
                     new Const(x), 
                     new Const(new Empty().add(new Integer(1))))
                 .accept(new SetEvalV()))
                .toStringS(" "));
        /* --- --- ---    THE SUN set, but set not his hope    --- --- --- */
        /*
        public String toString() { return "new " + getClass().getName() + "(" + ")"; }
        public String toString() { return "new " + getClass().getName() + "(" + x + ")"; }
        public String toString() { return "new " + getClass().getName() + "(" + x + ", " + y + ")"; }
        */
        /*
        abstract public String toStringS(String s);
        public String toStringS(String s) { return "new " + getClass().getName() + "(" + ")"; }
        public String toStringS(String s) { return "new " + getClass().getName() + "(" + t.toString() + ",\n" + s + r.toStringS(" "+s) + ")"; }
        public String toStringS(String s) { return "new " + getClass().getName() + "(\n" + l.toStringS(" "+s) + ",\n" + s + r.toStringS(" "+s) + ")"; }
        */
    }
}
