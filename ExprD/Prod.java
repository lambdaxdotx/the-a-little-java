
class Prod extends ExprD {
    ExprD l; ExprD r;
    Prod(ExprD _l, ExprD _r) { l = _l; r = _r; } //-------
    Object accept(ExprVisitorI ask) { return ask.forProd(l, r); }
}
