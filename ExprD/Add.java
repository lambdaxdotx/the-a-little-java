
class Add extends SetD {
    Integer i;
    SetD s;
    Add(Integer _i, SetD _s) { i = _i; s = _s; } //---
    boolean mem(Integer n) { 
        if (i.equals(n)) {
            return true;
        } else {
            return s.mem(n);
        } }       // this+t = this-{i} + t+{i}
    SetD plus(SetD t) { 
        return s.plus(t.add(i)); }
    SetD diff(SetD t) {  // t - {i} :
        if (t.mem(i)) {
            return s.diff(t);
        } else {
            return s.diff(t).add(i);
        } }
    SetD prod(SetD t) {  // t n {i} :
        if (t.mem(i)) {
            return s.prod(t).add(i);
        } else {
            return s.prod(t);
        } }
    public String toStringS(String str) { return "new " + getClass().getName() + "(" + i.toString() + ",\n" + str + s.toStringS(" "+str) + ")"; }
}
// diff = difference
// prod = intersect
// plus = union
