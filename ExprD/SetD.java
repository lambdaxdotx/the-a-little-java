
abstract class SetD {
    SetD add(Integer i) { // add a new element i
        if (mem(i)) {     // mem: isMemberOf
            return this;
        } else {
            return new Add(i, this);
        }
    } // ---------------------------
    abstract boolean mem(Integer i);
    abstract SetD plus(SetD s);
    abstract SetD diff(SetD s);
    abstract SetD prod(SetD s);
    abstract public String toStringS(String str);
}
