
class bIsSplitV implements bTreeVisitorI {
    public boolean forBud()                     { return true; }
    public boolean forFlat(FruitD f, TreeD t)   { return false; }
    public boolean forSplit(TreeD l, TreeD r)   { return l.accept(this) && r.accept(this); }
}
