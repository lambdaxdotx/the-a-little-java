
class IsSplitV implements TreeVisitorI {
    public Object forBud()                     { return new Boolean(true); }
    public Object forFlat(FruitD f, TreeD t)   { return new Boolean(false); }
    public Object forSplit(TreeD l, TreeD r)   { 
        return new Boolean(
                ((Boolean)(l.accept(this))).booleanValue() && 
                ((Boolean)(r.accept(this))).booleanValue()); }
}
