
abstract class TreeD {
    abstract boolean accept(bTreeVisitorI ask);
    abstract int     accept(iTreeVisitorI ask); // overloading
    abstract TreeD   accept(tTreeVisitorI ask);
    abstract Object  accept(TreeVisitorI ask);
    // -----------------------------
    abstract public String toStringS(String s);
}

// all TreeDs are either flat, split, or bud.
