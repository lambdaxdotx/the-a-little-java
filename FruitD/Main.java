
/* Chatper 7 */

class Main {
    public static void main(String args[]) {
        /* Here create the object y with which you wish to experiment ...  */
        System.out.println("\np.104");
        System.out.println( new Bud() .accept(new bIsSplitV()));
        System.out.println(
                new Split(
                    new Split(
                        new Bud(),
                        new Split(
                            new Bud(),
                            new Bud())),
                    new Split(
                        new Bud(),
                        new Split(
                            new Bud(),
                            new Bud())))
                .accept(new bIsSplitV()));

        System.out.println("\np.108");
        System.out.println(
                new Split(
                    new Bud(),
                    new Bud())
                .accept(new iHeightV()));
        System.out.println(
                new Split(
                    new Split(
                        new Bud(),
                        new Bud()),
                    new Flat(new Fig(),
                        new Flat(new Lemon(),
                            new Flat(new Apple(),
                                new Bud()))))
                .accept(new iHeightV()));
        System.out.println(
                new Bud()
                .accept(new iHeightV()));

        System.out.println("\np.109");
        System.out.println(
                new Split(
                    new Split(
                        new Flat(new Fig(),
                            new Bud()),
                        new Flat(new Fig(),
                            new Bud())),
                    new Flat(new Fig(),
                        new Flat(new Lemon(),
                            new Flat(new Apple(),
                                new Bud()))))
                .accept(
                    new tSubstV(
                        new Apple(),
                        new Fig()))
                .toStringS(" "));

        System.out.println("\np.111");
        System.out.println(
                new Split(
                    new Split(
                        new Flat(new Fig(),
                            new Bud()),
                        new Flat(new Fig(),
                            new Bud())),
                    new Flat(new Fig(),
                        new Flat(new Lemon(),
                            new Flat(new Apple(),
                                new Bud()))))
                .accept( new iOccursV( new Fig())));
        System.out.println( new Bud() .accept(new IsSplitV()));
        System.out.println(
                new Split(
                    new Split(
                        new Bud(),
                        new Split(
                            new Bud(),
                            new Bud())),
                    new Split(
                        new Bud(),
                        new Split(
                            new Bud(),
                            new Bud())))
                .accept(new IsSplitV()));

        /* --- --- ---    THE SUN set, but set not his hope    --- --- --- */
        /*
        public String toString() { return "new " + getClass().getName() + "(" + ")"; }
        public String toString() { return "new " + getClass().getName() + "(" + x + ")"; }
        public String toString() { return "new " + getClass().getName() + "(" + x + ", " + y + ")"; }
        */
        /*
        abstract public String toStringS(String s);
        public String toStringS(String s) { return "new " + getClass().getName() + "(" + ")"; }
        public String toStringS(String s) { return "new " + getClass().getName() + "(" + t.toString() + ",\n" + s + r.toStringS(" "+s) + ")"; }
        public String toStringS(String s) { return "new " + getClass().getName() + "(\n" + l.toStringS(" "+s) + ",\n" + s + r.toStringS(" "+s) + ")"; }
        */
    }
}
