
class Flat extends TreeD {
    FruitD f;
    TreeD t;
    Flat(FruitD _f, TreeD _t) {
        f = _f;
        t = _t;
    } // ----------------------
    boolean accept(bTreeVisitorI ask) { return ask.forFlat(f, t); }
    int     accept(iTreeVisitorI ask) { return ask.forFlat(f, t); }
    TreeD   accept(tTreeVisitorI ask) { return ask.forFlat(f, t); }
    Object  accept(TreeVisitorI ask)  { return ask.forFlat(f, t); }
    // ------------------------
    public String toStringS(String s) { return "new " + getClass().getName() + "(" + f.toString() + ",\n" + s + t.toStringS(" "+s) + ")"; }
}

