
class Split extends TreeD {
    TreeD l;
    TreeD r;
    Split(TreeD _l, TreeD _r) {
        l = _l;
        r = _r;
    } // ----------------------
    boolean accept(bTreeVisitorI ask) { return ask.forSplit(l, r); }
    int     accept(iTreeVisitorI ask) { return ask.forSplit(l, r); }
    TreeD   accept(tTreeVisitorI ask) { return ask.forSplit(l, r); }
    Object  accept(TreeVisitorI ask)  { return ask.forSplit(l, r); }
    public String toStringS(String s) { return "new " + getClass().getName() + "(\n" + l.toStringS(" "+s) + ",\n" + s + r.toStringS(" "+s) + ")"; }
}

