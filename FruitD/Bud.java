
class Bud extends TreeD {
    boolean accept(bTreeVisitorI ask) { return ask.forBud(); }
    int     accept(iTreeVisitorI ask) { return ask.forBud(); }
    TreeD   accept(tTreeVisitorI ask) { return ask.forBud(); }
    Object  accept(TreeVisitorI ask)  { return ask.forBud(); }
    // ------------------------------
    public String toStringS(String s) { return "new " + getClass().getName() + "(" + ")"; }
}
