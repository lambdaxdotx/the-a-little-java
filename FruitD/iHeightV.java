
class iHeightV implements iTreeVisitorI {
    public int forBud()                     { return 0; }
    public int forFlat(FruitD f, TreeD t)   { return 1 + t.accept(this); }
    public int forSplit(TreeD l, TreeD r)   { return 1 + Math.max(l.accept(this), r.accept(this)); }

}
