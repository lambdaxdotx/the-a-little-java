/* ShapeD
 *   | <-- Union   [ShapeD s, ShapeD t]
 */

class Union extends ShapeD {
    ShapeD s; ShapeD t;
    Union(ShapeD _s, ShapeD _t) { s=_s; t=_t; }
    // ----------------------------------------
    boolean accept(ShapeVisitorI ask) { return
        ((UnionVisitorI)ask).forUnion(s,t); }
    public String toStringS(String ind) { return getName(
            "\n" + ind + s.toStringS(" "+ind) + "," +
            "\n" + ind + t.toStringS(" "+ind)); }
}

