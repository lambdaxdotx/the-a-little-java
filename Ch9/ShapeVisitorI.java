interface ShapeVisitorI {
    boolean forCircle(int r);
    boolean forSquare(int s);
    boolean forTrans(PointD q, ShapeD s);
}

class HasPtV implements ShapeVisitorI {
    PointD p; HasPtV(PointD _p) { p=_p; }
    // ----------------------------------
    ShapeVisitorI newHasPt(PointD p) {
        return new HasPtV(p); }
    // ----------------------------------
    public boolean forCircle(int r) { return p.distanceToO() <= r; }
    public boolean forSquare(int s) { return p.x <= s && p.y <= s; }
    public boolean forTrans(PointD q, ShapeD s) {
        return s.accept(
                newHasPt(p.minus(q))); }
//              new HasPtV(p.minus(q))); }
}

