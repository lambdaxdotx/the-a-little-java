/* ShapeD
 *   | <-- Circle 
 *   | <-- Square
 *   | <-- Trans   [ShapeD s]
 */

abstract class ShapeD {
    // -------------------------
    abstract boolean accept(ShapeVisitorI ask);
    abstract public String toStringS(String ind);
    public String getName(String fields) { 
        return "new " + getClass().getName() + "("+fields+")"; }
}

class Circle extends ShapeD {
    int r; Circle(int _r) { r=_r; }
    // -------------------------
    boolean accept(ShapeVisitorI ask) { return ask.forCircle(r); }
    public String toStringS(String ind) { return getName(""+r); }
}

class Square extends ShapeD {
    int s; Square(int _s) { s=_s; }
    // -------------------------
    boolean accept(ShapeVisitorI ask) { return ask.forSquare(s); }
    public String toStringS(String ind) { return getName(""+s); }
}

class Trans extends ShapeD {
    PointD q; ShapeD s;
    Trans(PointD _q, ShapeD _s) { q=_q; s=_s; }
    // ----------------------------------------
    boolean accept(ShapeVisitorI ask) { return ask.forTrans(q,s); }
    public String toStringS(String ind) { return getName(
            "\n" + ind + q.toString() + "," +
            "\n" + ind + s.toStringS(" "+ind)); }
}

