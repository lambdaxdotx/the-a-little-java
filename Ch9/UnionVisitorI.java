interface UnionVisitorI extends ShapeVisitorI {
    boolean forUnion(ShapeD s, ShapeD t);
}

class UnionHasPtV extends HasPtV implements UnionVisitorI {
    UnionHasPtV(PointD _p) { super(_p); }
    // ---------------------------------
    ShapeVisitorI newHasPt(PointD p) {  // overriding the constructor!
        return new UnionHasPtV(p); }
    // ---------------------------------
    public boolean forUnion(ShapeD s, ShapeD t) {
        return  s.accept(this) || t.accept(this); }
}
