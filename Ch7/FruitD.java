/* FruitD
 *   | <-- Peach 
 *   | <-- Apple 
 *   | <-- Pear  
 *   | <-- Lemon 
 *   | <-- Fig   
 */

abstract class FruitD {
    // -------------------------
    abstract public String toStringS(String ind);
    public String getName(String fields) { return "new " + getClass().getName() + "("+fields+")"; }
}

class Peach extends FruitD {
    public boolean equals(Object o) {
        return (o instanceof Peach);
    }
    // -------------------------
    public String toStringS(String ind) { return getName(""); }
}

class Apple extends FruitD {
    public boolean equals(Object o) {
        return (o instanceof Apple);
    }
    // -------------------------
    public String toStringS(String ind) { return getName(""); }
}

class Pear extends FruitD {
    public boolean equals(Object o) {
        return (o instanceof Pear);
    }
    // -------------------------
    public String toStringS(String ind) { return getName(""); }
}

class Lemon extends FruitD {
    public boolean equals(Object o) {
        return (o instanceof Lemon);
    }
    // -------------------------
    public String toStringS(String ind) { return getName(""); }
}

class Fig extends FruitD {
    public boolean equals(Object o) {
        return (o instanceof Fig);
    }
    // -------------------------
    public String toStringS(String ind) { return getName(""); }
}

