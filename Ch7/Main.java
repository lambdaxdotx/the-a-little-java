
class Main {
    public static void main(String args[]) {
        /* Here create the object y with which you wish to experiment ...  */
        System.out.println("\np.99");
        TreeD x99 = 
            new Split(
                    new Bud(),
                    new Flat(new Fig(),
                        new Split(
                            new Bud(),
                            new Bud())));
        System.out.println(x99.toStringS(" "));
        TreeD y99 = 
            new Split(
                    new Split(
                        new Bud(),
                        new Flat(new Lemon(),
                            new Bud())),
                    new Flat(new Fig(),
                        new Split(
                            new Bud(),
                            new Bud())));
        System.out.println(y99.toStringS(" "));

        System.out.println("\np.104");
        TreeD x104 = 
            new Split(
                    new Split(
                        new Bud(),
                        new Split(
                            new Bud(),
                            new Bud())),
                    new Split(
                        new Bud(),
                        new Split(
                            new Bud(),
                            new Bud())));
        System.out.println(x104.toStringS(" "));
        System.out.println(".accept( new IsSplitV() )?");
        System.out.println(x104.accept( new IsSplitV() ));
        System.out.println(".accept( new HasFruitV() )?");
        System.out.println(x104.accept( new HasFruitV() ));
//
//        System.out.println("\np.95");
//        PieD z95 =
//            new Top(new Anchovy(),
//                    new Top(new Tuna(),
//                        new Top(new Anchovy(),
//                            new Top(new Tuna(),
//                                new Top(new Anchovy(),
//                                    new Bot())))));
//        System.out.println(z95.toStringS(" "));
//        System.out.println(".accept(new LtdSubstV(2, new Salmon(), new Anchovy()))?");
//        System.out.println(z95.accept(
//                    new LtdSubstV( 2, 
//                        new Salmon(), 
//                        new Anchovy())).toStringS(" "));
//
        /* --- --- THE SUN set, but set not his hope --- --- */
        /*
           abstract public String toStringS(String ind);
           public String getName(String fields) { return "new " + getClass().getName() + "(" + fields + ")"; }
           public String toStringS(String ind) { return getName(""); }
           public String toStringS(String ind) { return getName("\n"+ind + s.toStringS(" "+ind)); }
        */
    }
}
