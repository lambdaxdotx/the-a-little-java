
class IsSplitV implements TreeVisitorI {
    public Object forBud() { return new Boolean(true); }
    public Object forFlat(FruitD f, TreeD t) { return new Boolean(false); }
    public Object forSplit(TreeD l, TreeD r) {
        if ( ((Boolean)l.accept(this)) )
            return r.accept(this);
        else
            return new Boolean(false); }
}
