
class HasFruitV implements TreeVisitorI {
    public Object forBud() { return new Boolean(false); }
    public Object forFlat(FruitD f, TreeD t) { return new Boolean(true); }
    public Object forSplit(TreeD l, TreeD r) {
        if ( ((Boolean)(l.accept(this))) )
            return new Boolean(true);
        else
            return r.accept(this); }
}
