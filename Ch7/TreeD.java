
abstract class TreeD {
    abstract Object accept(TreeVisitorI ask); // producing booleans
    // ---------------------------------------
    abstract public String toStringS(String ind);
    public String getName(String fields) { return "new " + getClass().getName() + "(" + fields + ")"; }
}

class Bud extends TreeD {
    ;
    // -----------------------------------------
    Object accept (TreeVisitorI ask) { return ask.forBud(); }
    // --------------------------------------------------------
    public String toStringS(String ind) { return getName(""); }
}

class Flat extends TreeD {
    FruitD f;
    TreeD  t;
    Flat(FruitD _f, TreeD _t) { f = _f; t = _t; }
    // -----------------------------------------
    Object accept (TreeVisitorI ask) { return ask.forFlat(f, t); }
    // -----------------------------------------------------*--*----
    public String toStringS(String ind) {
        return getName(
                f.toStringS("") + ",\n" + ind + 
                t.toStringS(" "+ind) ); }
}

class Split extends TreeD {
    TreeD  l;
    TreeD  r;
    Split(TreeD _l, TreeD _r) { l = _l; r = _r; }
    // -----------------------------------------
    Object accept (TreeVisitorI ask) { return ask.forSplit(l, r); }
    // ------------------------------------------------------*--*---
    public String toStringS(String ind) {
        return getName( 
                "\n" + ind + l.toStringS(" "+ind) + ", "+
                "\n" + ind + r.toStringS(" "+ind) ); }
}

