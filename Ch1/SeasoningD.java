/* SeasoningD
 *     |<-- Salt
 *     |<-- Pepper
 *     |<-- Thyme
 *     |<-- Sage */

abstract class SeasoningD {
}

class Salt extends SeasoningD {
}

class Pepper extends SeasoningD {
}

class Thyme extends SeasoningD {
}

class Sage extends SeasoningD {
}
