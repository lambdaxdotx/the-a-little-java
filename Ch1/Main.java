
class Main {
    public static void main(String args[]) {
        /* Here create the object y with which you wish to experiment ...  */
        System.out.println("\np.10");
        System.out.println( new Salt() instanceof SeasoningD );
        System.out.println( new Pepper() instanceof SeasoningD );

        System.out.println("\np.12");
        System.out.println( new ManhattanPt(2,3) instanceof ManhattanPt );
        
        System.out.println("\np.13");
        System.out.println( new Zero() instanceof NumD );
        System.out.println( 
                new OneMoreThan(
                    new Zero()) instanceof NumD );
        
        System.out.println("\np.14");
        System.out.println(
                new OneMoreThan(
                    new OneMoreThan(
                        new Zero()))
                .toStringS(" "));

        System.out.println("\np.16");
        System.out.println( new Base( new Zero()) instanceof Base );
        System.out.println( new Base( new Zero()) instanceof LayerD );
        System.out.println( new Base( new Zero()) instanceof Object );
        System.out.println( new Base( new Salt()) instanceof Base );
        System.out.println( new Base( new Salt()) instanceof LayerD );
        System.out.println( new Base( new Salt()) instanceof Object );

        System.out.println("\np.16");
        System.out.println(
                new Base(
                    new Integer(5))
                .toStringS(" "));
        System.out.println(
                new Base(
                    new Boolean(false))
                .toStringS(" "));
        /* --- --- ---    THE SUN set, but set not his hope    --- --- --- */
        /*
        public String toString() { return "new " + getClass().getName() + "(" + ")"; }
        public String toString() { return "new " + getClass().getName() + "(" + x + ")"; }
        public String toString() { return "new " + getClass().getName() + "(" + x + ", " + y + ")"; }
        */
    }
}
