/* LayerD
 *   |<-- Base
 *   |<-- Slice
 */

abstract class LayerD {
   abstract public String toStringS(String s); 
}

class Base extends LayerD {
    Object o;
    Base(Object _o) { o = _o; }
    // -------------------------
    public String toStringS(String s) { return 
        "new " + getClass().getName() + "(" + o.toString() + ")"; }
}

class Slice extends LayerD {
    LayerD l;
    Slice(LayerD _l) { l = _l; }
    // -------------------------
    public String toStringS(String s) { return 
        "new " + getClass().getName() + "(" + "\n" + 
            s + l.toStringS(" "+s) + ")"; }
}
