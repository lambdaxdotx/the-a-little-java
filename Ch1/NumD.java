/*
 * NumD
 *  |<-- Zero
 *  |<-- OneMoreThan
 */

abstract class NumD {
    abstract public String toStringS(String s);
}

class Zero extends NumD {
    public boolean equals(Object o) {
        return (o instanceof Zero); }
    public String toStringS(String s) { return 
        "new " + getClass().getName() + "(" + ")"; }
}

class OneMoreThan extends NumD {
    NumD predecessor;
    OneMoreThan(NumD _p) { predecessor = _p; }
    // ---------------------------------------
    public boolean equals(Object o) {
        if (o instanceof OneMoreThan) {
            return // this == o iff this.predecessor == o.predecessor
                predecessor.equals( ((OneMoreThan)o).predecessor );
        } else {
            return false;
        } }
    public String toStringS(String s) { return 
        "new " + getClass().getName() + "(" + "\n" + 
            s + predecessor.toStringS(" "+s) + ")"; }
}
