
class Main {
    public static void main(String args[]) {
        /* Here create the object y with which you wish to experiment ...  */
        System.out.println("\np.90");
        PieD z90 = 
            new Top(new Integer(3),
                    new Top(new Integer(2),
                        new Top(new Integer(3),
                            new Bot())));
        System.out.println(z90.toStringS(" "));
        System.out.println(".accept(new SubstV(new Integer(5), new Integer(3)))?");
        System.out.println(z90.accept(
                new SubstV(
                    new Integer(5), 
                    new Integer(3))).toStringS(" "));

        System.out.println("\np.95");
        PieD z95 =
            new Top(new Anchovy(),
                    new Top(new Tuna(),
                        new Top(new Anchovy(),
                            new Top(new Tuna(),
                                new Top(new Anchovy(),
                                    new Bot())))));
        System.out.println(z95.toStringS(" "));
        System.out.println(".accept(new LtdSubstV(2, new Salmon(), new Anchovy()))?");
        System.out.println(z95.accept(
                    new LtdSubstV( 2, 
                        new Salmon(), 
                        new Anchovy())).toStringS(" "));

        /* --- --- THE SUN set, but set not his hope --- --- */
        /*
           abstract public String toStringS(String ind);
           public String getName(String fields) { return "new " + getClass().getName() + "(" + fields + ")"; }
           public String toStringS(String ind) { return getName(""); }
           public String toStringS(String ind) { return getName("\n"+ind + s.toStringS(" "+ind)); }
        */
    }
}
