
abstract class PieD {
    abstract PieD accept(PieVisitorI ask);
    // ---------------------------------------
    abstract public String toStringS(String ind);
    public String getName(String fields) { return "new " + getClass().getName() + "(" + fields + ")"; }
}

class Bot extends PieD {
    PieD accept (PieVisitorI ask) { return ask.forBot(); }
    // ---------------------------------------
    public String toStringS(String ind) { return getName(""); }
}

class Top extends PieD {
    PieD r;
    Object t;
    Top(Object _t, PieD _p) {
        r = _p; 
        t = _t;
    } // --------------------
    PieD accept (PieVisitorI ask) { return ask.forTop(t, r); }
    // ---------------------------------------
    public String toStringS(String ind) { return getName( t.toString() + ",\n" +
            ind + r.toStringS(" "+ind) ); }
}
