/* ShishD
 *   | <-- Skewer 
 *   | <-- Onion    [ShishD s]
 *   | <-- Lamb     [ShishD s]
 *   | <-- Tomato   [ShishD s]
 */

abstract class ShishD {
    // -------------------------
    OnlyOnionsV ooFn = new OnlyOnionsV();
    IsVegetarianV ivFn = new IsVegetarianV();
    abstract boolean onlyOnions();
    abstract boolean isVegetarian();
    abstract public String toStringS(String ind);
    public String getName(String fields) { return "new " + getClass().getName() + "("+fields+")"; }
}

class Skewer extends ShishD {
    // -------------------------
    boolean onlyOnions() { return ooFn.forSkewer(); }
    boolean isVegetarian() { return ivFn.forSkewer(); }
    public String toStringS(String ind) { return getName(""); }
}

class Onion extends ShishD {
    ShishD s;
    Onion(ShishD _s) { s = _s; }
    // -------------------------
    boolean onlyOnions() { return ooFn.forOnion(s); }
    boolean isVegetarian() { return ivFn.forOnion(s); }
    public String toStringS(String ind) { return getName("\n"+ind + s.toStringS(" "+ind)); }
}

class Lamb extends ShishD {
    ShishD s;
    Lamb(ShishD _s) { s = _s; }
    // -------------------------
    boolean onlyOnions() { return ooFn.forLamb(s); }
    boolean isVegetarian() { return ivFn.forLamb(s); }
    public String toStringS(String ind) { return getName("\n"+ind + s.toStringS(" "+ind)); }
}

class Tomato extends ShishD {
    ShishD s;
    Tomato(ShishD _s) { s = _s; }
    // -------------------------
    boolean onlyOnions() { return ooFn.forTomato(s); }
    boolean isVegetarian() { return ivFn.forTomato(s); }
    public String toStringS(String ind) { return getName("\n"+ind + s.toStringS(" "+ind)); }
}

