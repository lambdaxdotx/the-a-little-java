
class SubstV implements PieVisitorI {
    Object n; Object o;
    SubstV(Object _n, Object _o) { n=_n; o=_o; }
    // -----------------------------------------
    public PieD forBot()                 { return new Bot(); }
    public PieD forTop(Object t, PieD r) {
        if (o.equals(t)) {
            return new Top(n, r.accept(this));
        } else {
            return new Top(t, r.accept(this));
        } }
}
