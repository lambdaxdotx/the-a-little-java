
class Main {
    public static void main(String args[]) {
        /* Here create the object y with which you wish to experiment ...  */
        System.out.println("\np.17");
        System.out.println(
                new Onion(
                    new Lamb(
                        new Onion(
                            new Skewer())))
                .toStringS(" "));

        System.out.println("\np.19");
        Onion z19 = 
            new Onion(
                new Onion(
                    new Skewer()));
        System.out.println(z19.toStringS(" "));
        System.out.println(".onlyOnions()?");
        System.out.println(z19.onlyOnions());

        System.out.println("\np.22");
        Onion z22 = 
            new Onion(
                new Lamb(
                    new Skewer()));
        System.out.println(z22.toStringS(" "));
        System.out.println(".onlyOnions()?");
        System.out.println(z22.onlyOnions());

        System.out.println("\np.31");
        Shallot z31 = 
            new Shallot(
                new Radish(
                    new Holder(
                        new Dagger())));
        System.out.println(z31.toStringS(" "));
        System.out.println(".isVeggie()?");
        System.out.println(z31.isVeggie());

        System.out.println("\np.32");
        Shallot z32 = 
            new Shallot(
                new Radish(
                    new Holder(
                        new Gold())));
        System.out.println(z32.toStringS(" "));
        System.out.println(".whatHolder()?");
        System.out.println(z32.whatHolder().toString());

        System.out.println("\np.33");
        Shallot z33 = 
            new Shallot(
                new Radish(
                    new Holder(
                        new Integer(68))));
        System.out.println(z33.toStringS(" "));
        System.out.println(".whatHolder()?");
        System.out.println(z33.whatHolder().toString());

        System.out.println("\np.38");
        CartesianPt z38 = 
            new CartesianPt(12,5);
        System.out.println(z38.toString());
        System.out.println(".closerToO(new CartesianPt(3,4))?");
        System.out.println(z38.closerToO(new CartesianPt(3,4)));
        /* --- --- THE SUN set, but set not his hope --- --- */
        /*
           abstract public String toStringS(String ind);
           public String getName(String fields) { return "new " + getClass().getName() + "(" + fields + ")"; }
           public String toStringS(String ind) { return getName(""); }
           public String toStringS(String ind) { return getName("\n"+ind + s.toStringS(" "+ind)); }
        */
    }
}
