
abstract class PlateD {
    public String getName(String fields) { return "new " + getClass().getName() + "(" + fields + ")"; }
    public String toString() { return getName(""); }
}

class Gold extends PlateD {}

class Silver extends PlateD {}

class Brass extends PlateD {}

class Copper extends PlateD {}

class Wood extends PlateD {}
