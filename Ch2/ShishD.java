/* ShishD
 *   | <-- Skewer 
 *   | <-- Onion    [ShishD s]
 *   | <-- Lamb     [ShishD s]
 *   | <-- Tomato   [ShishD s]
 */

abstract class ShishD {
    // -------------------------
    abstract boolean onlyOnions();
    abstract boolean isVegetarian();
    abstract public String toStringS(String ind);
    public String getName(String fields) { return "new " + getClass().getName() + "("+fields+")"; }
}

class Skewer extends ShishD {
    // -------------------------
    boolean onlyOnions() { return true; }
    boolean isVegetarian() { return true; }
    public String toStringS(String ind) { return getName(""); }
}

class Onion extends ShishD {
    ShishD s;
    Onion(ShishD _s) { s = _s; }
    // -------------------------
    boolean onlyOnions() { return s.onlyOnions(); }
    boolean isVegetarian() { return s.isVegetarian(); }
    public String toStringS(String ind) { return getName("\n"+ind + s.toStringS(" "+ind)); }
}

class Lamb extends ShishD {
    ShishD s;
    Lamb(ShishD _s) { s = _s; }
    // -------------------------
    boolean onlyOnions() { return false; }
    boolean isVegetarian() { return false; }
    public String toStringS(String ind) { return getName("\n"+ind + s.toStringS(" "+ind)); }
}

class Tomato extends ShishD {
    ShishD s;
    Tomato(ShishD _s) { s = _s; }
    // -------------------------
    boolean onlyOnions() { return false; }
    boolean isVegetarian() { return s.isVegetarian(); }
    public String toStringS(String ind) { return getName("\n"+ind + s.toStringS(" "+ind)); }
}

