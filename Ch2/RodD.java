
abstract class RodD {
    public String getName(String fields) { return "new " + getClass().getName() + "(" + fields + ")"; }
    public String toString() { return getName(""); }
}

class Dagger extends RodD {}

class Sabre extends RodD {}

class Sword extends RodD {}
