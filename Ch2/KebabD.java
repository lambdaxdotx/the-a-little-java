/* KebabD
 *   | <-- Holder   [Object o] 
 *   | <-- Shallot  [KebabD s]
 *   | <-- Shrimp   [KebabD s]
 *   | <-- Radish   [KebabD s]
 *   | <-- Pepper   [KebabD s]
 *   | <-- Zucchini [KebabD s]
 */

abstract class KebabD {
    // -------------------------
    abstract boolean isVeggie();
    abstract Object  whatHolder();
    abstract public String toStringS(String ind);
    public String getName(String fields) { return "new " + getClass().getName() + "("+fields+")"; }
}

class Holder extends KebabD {
    Object o;
    Holder(Object _o) { o = _o; }
    // -------------------------
    boolean isVeggie()  { return true; }
    Object whatHolder() { return o; }
    public String toStringS(String ind) { return getName(o.toString()); }
}

class Shallot extends KebabD {
    KebabD k;
    Shallot(KebabD _k) { k = _k; }
    // -------------------------
    boolean isVeggie()  { return k.isVeggie(); }
    Object whatHolder() { return k.whatHolder(); }
    public String toStringS(String ind) { return getName("\n"+ind + k.toStringS(" "+ind)); }
}

class Shrimp extends KebabD {
    KebabD k;
    Shrimp(KebabD _k) { k = _k; }
    // -------------------------
    boolean isVeggie()  { return false; }
    Object whatHolder() { return k.whatHolder(); }
    public String toStringS(String ind) { return getName("\n"+ind + k.toStringS(" "+ind)); }
}

class Radish extends KebabD {
    KebabD k;
    Radish(KebabD _k) { k = _k; }
    // -------------------------
    boolean isVeggie()  { return k.isVeggie(); }
    Object whatHolder() { return k.whatHolder(); }
    public String toStringS(String ind) { return getName("\n"+ind + k.toStringS(" "+ind)); }
}

class Pepper extends KebabD {
    KebabD k;
    Pepper(KebabD _k) { k = _k; }
    // -------------------------
    boolean isVeggie()  { return k.isVeggie(); }
    Object whatHolder() { return k.whatHolder(); }
    public String toStringS(String ind) { return getName("\n"+ind + k.toStringS(" "+ind)); }
}

class Zucchini extends KebabD {
    KebabD k;
    Zucchini(KebabD _k) { k = _k; }
    // -------------------------
    boolean isVeggie()  { return k.isVeggie(); }
    Object whatHolder() { return k.whatHolder(); }
    public String toStringS(String ind) { return getName("\n"+ind + k.toStringS(" "+ind)); }
}
